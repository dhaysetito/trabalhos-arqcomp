#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define OK 0

double *matrizA, *matrizB, *matrizC;

/*Multiplica duas matrizes A e B de ordem n*/ 
void dgemm(int n, double* A, double* B, double* C) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            double cij = 0.0; /* Iniciar cij em 0.0 */
            for (int k = 0; k < n; ++k) {
                cij += A[i * n + k] * B[k * n + j]; /* cij += A[i][k]*B[k][j] */
            }
            C[i * n + j] = cij; /* C[i][j] = cij */
        }
    }
}

/* Cria uma matriz X de ordem n */
void criar_matriz(int n, double* A) {
    for (int i = 0; i < n * n; ++i) {
        A[i] = rand() / (double)RAND_MAX; /* Gerar valores aleatórios entre 0 e 1 */
    }
}

/* Gera e aloca 3 matrizes de ordem n*/
void criar_matrizes_input(int ordem) {
    posix_memalign((void**)&matrizA, 32, ordem * ordem * sizeof(double));
    posix_memalign((void**)&matrizB, 32, ordem * ordem * sizeof(double));
    posix_memalign((void**)&matrizC, 32, ordem * ordem * sizeof(double));

    if (matrizA == NULL || matrizB == NULL || matrizC == NULL) {
        fprintf(stderr, "Erro na alocação de memória\n");
        exit(1);
    }

    criar_matriz(ordem, matrizA);
    criar_matriz(ordem, matrizB);
}

/*Libera a memoria da matriz*/
void liberar_matrizes() {
    free(matrizA);
    free(matrizB);
    free(matrizC);
}

/* Testa se a multplicacao estah correta*/
void teste_matriz(int ordem, double* matrizA, double* matrizB, double* matrizC) {
    // Teste de matriz
    printf("Matriz A ---------- \n");
    for (int i = 0; i < ordem; i++) {
        for (int j = 0; j < ordem; j++) {
            printf("%f ", matrizA[i * ordem + j]);
        }
        printf("\n"); // Adiciona uma nova linha após cada linha da matriz 
    }
    printf("----------------------------------\n\n");
    printf("Matriz B ---------- \n");
    for (int i = 0; i < ordem; i++) {
        for (int j = 0; j < ordem; j++) {
            printf("%f ", matrizB[i * ordem + j]);
        }
        printf("\n"); // Adiciona uma nova linha após cada linha da matriz 
    }
    printf("----------------------------------");
    printf("Matriz C ---------- \n\n");

    for (int i = 0; i < ordem; i++) {
        for (int j = 0; j < ordem; j++) {
            printf("%f ", matrizC[i * ordem + j]);
        }
        printf("\n"); // Adiciona uma nova linha após cada linha da matriz 
    }
    printf("----------------------------------\n\n");
}

/* Executa a multplicacao para diferentes ordens e salvar o tempo*/
void executar_testes(int* lista, int tamanho_lista, double* tempos_medios, double* desvio_padrao) {
    for (int i = 0; i < tamanho_lista; ++i) {
        int n = lista[i];
        printf("\nExecutando multiplicacao de matrizes de ordem %d...\n", n);

        double soma_tempos = 0.0;
        double tempos[5]; // Para armazenar os tempos de execução
        int rep = sizeof(tempos) / sizeof(tempos[0]);

        printf("Tempo ordem: %d |T(s)={", n);

        for (int j = 0; j < rep; ++j) {
            criar_matrizes_input(n);
            clock_t tempo_de_inicio = clock();
            dgemm(n, matrizA, matrizB, matrizC);
            clock_t tempo_de_execucao = clock() - tempo_de_inicio;
            soma_tempos += ((double) tempo_de_execucao) / CLOCKS_PER_SEC;
            tempos[j] = ((double) tempo_de_execucao) / CLOCKS_PER_SEC;
            liberar_matrizes();
            printf("%0.8f,",tempos[j]);
        }
        printf("}\n");

        tempos_medios[i] = soma_tempos / rep;

        // Calcular o desvio padrão
        double soma_quad_dif = 0.0;
        for (int k = 0; k < rep; ++k) {
            soma_quad_dif += pow(tempos[k] - tempos_medios[i], 2);
        }
        desvio_padrao[i] = sqrt(soma_quad_dif / rep);

        printf("Tempo medio: %0.5f s | ", tempos_medios[i]);
        printf("Desvio padrao: %0.5f s\n", desvio_padrao[i]);
    }
}

void salvar_tempos_em_csv(const char* nome_arquivo, int* lista_ordens, int tamanho_lista, double* tempos_medios, double* desvio_padrao) {
    FILE* arquivo = fopen(nome_arquivo, "w");
    if (arquivo == NULL) {
        fprintf(stderr, "Erro ao abrir o arquivo para escrita\n");
        exit(1);
    }

    fprintf(arquivo, "Ordem,Tempo Medio(s),Desvio padrão(s)\n");
    for (int i = 0; i < tamanho_lista; ++i) {
        fprintf(arquivo, "%d,%0.5f,%0.5f\n", lista_ordens[i], tempos_medios[i], desvio_padrao[i]);
    }

    fclose(arquivo);
    printf("\nTempos medios salvos no arquivo %s\n", nome_arquivo);
}


int main() {
    int lista_ordens[] = {128, 256, 512, 1024, 2048, 4096};
    int tamanho_lista = sizeof(lista_ordens) / sizeof(lista_ordens[0]);
    double tempos_medios[tamanho_lista];
    double desvio_padrao[tamanho_lista];

    executar_testes(lista_ordens, tamanho_lista, tempos_medios, desvio_padrao);

    salvar_tempos_em_csv("tempos_medios_c_nao_otimizado_O3.csv", lista_ordens, tamanho_lista, tempos_medios, desvio_padrao);

    printf("\nTempos medios e desvio para cada ordem de matriz: \n");
    for (int i = 0; i < tamanho_lista; ++i) {
        printf("%d: %0.5f segundos (Desvio Padrao: %0.5f)\n", lista_ordens[i], tempos_medios[i], desvio_padrao[i]);
    }
    printf("\n");
    return OK;
}