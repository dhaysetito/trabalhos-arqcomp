import time
import numpy as np
import csv
import math
import random

'''Funcao que retorna uma lista com 5 tempos de multiplicacao de uma matriz'''
def multiply_matriz(n):
    A = np.random.uniform(0, 1, (n, n))
    B = np.random.uniform(0, 1, (n, n))
    
    time_list = []
    init_time = 0
    ''' Multplica as matrizes'''

    for times in range(15):
        init_time = time.time()
        C = np.dot(A, B)
        delta_time = time.time() - init_time
        if delta_time != 0.0:
            time_list.append(delta_time)
        
    
    return time_list

'''Funcao que retorna tempo do DGEMM'''
def time_average(time_list):
    time_average = []
    time_std = []
    time_total_list = []
    time_total = 0

    for i in time_list:
        time_aux_average = 0
        for j in i:
            time_total+=j
            time_aux_average+=j
        
        time_total_list.append("{:.5f}".format(time_aux_average))   #Adiciona a soma dos tempos numa lista
        time_aux_average /= len(i)  #Faz a media
        
        #Calculo do desvio padrao
        diff_2 = [(x - time_aux_average) ** 2 for x in i]
        variance = sum(diff_2) / (len(i) - 1) 
        std_average = math.sqrt(variance)

        time_average.append("{:.5f}".format(time_aux_average))
        time_std.append("{:.5f}".format(std_average))

    return time_average, time_std, time_total,time_total_list

'''Salva tempo em um csv'''
def save_to_csv(n_values, time_averages, time_std_list, filename="tempo_medio"):
    with open(filename, mode='w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(['Ordem', 'Tempo Medio(s)', 'Desvio padrão(s)'])
        for n, avg_time, std_time in zip(n_values, time_averages, time_std_list):
            writer.writerow([n, avg_time, std_time])
    print(f"Arquivo {filename} salvo com sucesso.")

n = [128, 256, 512, 1024, 2048]
#n = [4]

time_list = []

if __name__ == "__main__":
    for i in n:
        print("Iniciando multiplicação de matriz de ordem: ", i)
        time_list.append(multiply_matriz(i))

    time_average_list, time_std_list, time_total,time_total_list = time_average(time_list)

    print(f"Ordem: {n} \nLista de tempos: {time_list} \nLista tempo médio: {time_average_list} \n" \
          f"Lista do desvio padrão: {time_std_list} \nTempo total: {time_total:.5f}\n")

    save_to_csv(n, time_average_list, time_std_list, filename='tempos_medios_python_otimizado.csv')