import random
import time
import csv
import math

'''Funcao que retorna uma lista com 5 tempos de multiplicacao de uma matriz'''
def multiply_matriz(n):
    A = [[random.uniform(0, 1) for _ in range(n)] for _ in range(n)]
    B = [[random.uniform(0, 1) for _ in range(n)] for _ in range(n)]

    # Inicializar a matriz C com zeros
    C = [[0 for _ in range(n)] for _ in range(n)]

    #print(f'Matriz A:\n{A}\nMatrizB\n{B}')

    print(f"Executando multiplicacao de matrizes de ordem {n} ...");
    time_list = []
    for times in range(5):
        init_time = time.time()
        for i in range(n):
            for j in range(n):
                for k in range(n):
                    C[i][j] += A[i][k] * B[k][j] 
        delta_time = time.time() - init_time
        time_list.append(delta_time)

    return time_list

'''Funcao que retorna tempo do DGEMM'''
def time_average(time_list):
    time_average = []
    time_std = []
    time_total_list = []
    time_total = 0
    for i in time_list:
        time_aux_average = 0
        for j in i:
            time_total+=j
            time_aux_average+=j

        time_total_list.append("{:.5f}".format(time_aux_average))   #Adiciona a soma dos tempos numa lista
        time_aux_average /= len(i)  #Faz a media
        
        #Calculo do desvio padrao
        diff_2 = [(x - time_aux_average) ** 2 for x in i]
        variance = sum(diff_2) / (len(i) - 1) 
        std_average = math.sqrt(variance)

        time_average.append("{:.5f}".format(time_aux_average))
        time_std.append("{:.5f}".format(std_average))

    return time_average, time_std, time_total,time_total_list

'''Salva tempo em um csv'''
def save_to_csv(n_values, time_averages, time_std_list, filename="tempo_medio"):
    with open(filename, mode='w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(['Ordem', 'Tempo Medio(s)', 'Desvio padrão(s)'])
        for n, avg_time, std_time in zip(n_values, time_averages, time_std_list):
            writer.writerow([n, avg_time, std_time])
    print(f"Arquivo {filename} salvo com sucesso.")

n = [128, 256, 512, 1024, 2048, 4096]
#n = [4]

time_list = []

if __name__ == "__main__":
    for i in n:
        time_list.append(multiply_matriz(i))

    time_average_list, time_std_list, time_total,time_total_list = time_average(time_list)

    print(f"Ordem: {n} \nLista de tempos: {time_list} \nLista tempo médio: {time_average_list} \n" \
          f"Lista do desvio padrão: {time_std_list} \nTempo total: {time_total:.5f}\n")

    save_to_csv(n, time_average_list, time_std_list, filename='tempos_medios_python_nao_otimizado_teste.csv')