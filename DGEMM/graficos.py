import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import os

# Função para ler múltiplos arquivos CSV e plotar gráfico de barras múltiplas
def plot_multiple_bar_chart(csv_files, output_file, name_columns):
    if len(csv_files) != len(name_columns):
        raise ValueError("O número de arquivos CSV deve ser igual ao número de nomes de colunas fornecidos.")
    
    combined_data = pd.DataFrame()

    # Lê cada arquivo CSV e combina os dados
    for csv_file, col_name in zip(csv_files, name_columns):
        data = pd.read_csv(csv_file)
        data.columns = [data.columns[0], col_name, f'{col_name}_error']  # Renomeia a segunda coluna com o nome fornecido
        if combined_data.empty:
            combined_data = data[[data.columns[0], col_name]]
            combined_errors = data[[data.columns[0], f'{col_name}_error']]
        else:
            combined_data = pd.merge(combined_data, data[[data.columns[0], col_name]], on=data.columns[0])
            combined_errors = pd.merge(combined_errors, data[[data.columns[0], f'{col_name}_error']], on=data.columns[0])



    # Define os cabeçalhos das colunas
    col1_header = combined_data.columns[0]

    # Configura o gráfico de barras múltiplas
    num_files = len(csv_files)
    bar_width = 0.8 / num_files  # Largura das barras
    indices = np.arange(len(combined_data))

    plt.figure(figsize=(12, 8))
    for i, col in enumerate(combined_data.columns[1:]):
        errors = combined_errors[f'{col}_error']
        bars = plt.bar(indices + i * bar_width, 
                       combined_data[col], 
                       bar_width,
                       yerr=errors, 
                       capsize=5,
                       label=col)
        
        # Adiciona rótulos de valor nas barras
        for bar in bars:
            height = bar.get_height()
            plt.annotate(f'{height:.3f}',
                         xy=(bar.get_x() + bar.get_width() / 2, height),
                         xytext=(0, 3),  # 3 points vertical offset
                         textcoords="offset points",
                         ha='center', va='bottom',
                         fontsize=7)

    plt.xlabel(col1_header)
    plt.ylabel('Tempo médio (s)')
    plt.title('Tempo de Processamento - DGEMM')
    plt.xticks(indices + bar_width * (num_files - 1) / 2, combined_data[col1_header])
    plt.yscale('log')  # Usar escala logarítmica para melhor visualização
    plt.legend(title='Implementações')

    # Salva o gráfico como um arquivo PNG
    plt.savefig(output_file, format='png')
    plt.close()  # Fecha a figura para liberar memória


def salva_graficos(csv_files, output_files, name_columns):
    
    plot_multiple_bar_chart(csv_files, output_files, name_columns)
    
    print(f"Gráfico salvo como {output_files}")


def divide_csv_values(files, output_file):
    # Ler os arquivos CSV
    df1 = pd.read_csv(files[0])
    df2 = pd.read_csv(files[1])
    df3 = pd.read_csv(files[2])
    df4 = pd.read_csv(files[3])
    df5 = pd.read_csv(files[4])
    df6 = pd.read_csv(files[5])

    # Verificar se todos os arquivos têm o mesmo número de linhas
    if not (len(df1) == len(df2) == len(df3) == len(df4) == len(df5)):
        raise ValueError("Todos os arquivos CSV devem ter o mesmo número de linhas.")

    # Criar um DataFrame para armazenar os resultados
    result = pd.DataFrame()
    result[df1.columns[0]] = df1[df1.columns[0]] 

    dataframes = [df1, df2, df3, df4, df5, df6]
    names = ['Python não otimizado', 'Python otimizado', 'C não otimizado', 'C com subwords', 'C com unroll', 'C com multithreads']

    # Iterar sobre os DataFrames e calcular a divisão das colunas
    for df, name in zip(dataframes, names):
        result[name] = (df1[df1.columns[1]] / df[df.columns[1]]).astype(int)

    result.to_csv(output_file + '.csv', index=False)

    for df, name in zip(dataframes, names):
        x = df1[df1.columns[1]]
        y = df[df.columns[1]]
        x_err = df1[df1.columns[2]]
        y_err = df[df.columns[2]]
        result[name] = ((x / y) * np.sqrt((x_err / x) ** 2 + (y_err / y) ** 2)).astype(int)
    

    # Salvar o resultado em um novo arquivo CSV
    result.to_csv(output_file + '_err.csv', index=False)
    print('Arquivo gerado.\n')


# Lista de arquivos CSV
csv_files = ['Dados/Resultados/tempos_medios_python_nao_otimizado.csv',
             'Dados/Resultados/tempos_medios_python_otimizado.csv',
             'Dados/Resultados/tempos_medios_c_nao_otimizado_O0.csv',
             'Dados/Resultados/tempos_medios_c_nao_otimizado_O1.csv', 
             'Dados/Resultados/tempos_medios_c_nao_otimizado_O2.csv', 
             'Dados/Resultados/tempos_medios_c_nao_otimizado_O3.csv', 
             'Dados/Resultados/tempos_medios_c_subword.csv',
             'Dados/Resultados/tempos_medios_c_unroll.csv',
             'Dados/Resultados/tempos_medios_c_blocking.csv',
             'Dados/Resultados/tempos_medios_c_multithread.csv']

# Lista de nomes para as colunas
name_columns = ['Python não otimizado',
                'Python otimizado',
                'C não otimizado - O0', 
                'C não otimizado - O1', 
                'C não otimizado - O2',
                'C não otimizado - O3',
                'C com paralelismo subword',
                'C com unroll',
                'C com blocking',
                'C com multithread']

# Nome do arquivo de saída (gráfico PNG)

output_files = ['Dados/Graficos/Novo/grafico_python_nao_otimizado.png',
                'Dados/Graficos/Novo/grafico_python_numpy.png',
                'Dados/Graficos/Novo/grafico_c_nao_otimizado_O0.png',
                'Dados/Graficos/Novo/grafico_c_nao_otimizado_O1.png',
                'Dados/Graficos/Novo/grafico_c_nao_otimizado_O2.png',
                'Dados/Graficos/Novo/grafico_c_nao_otimizado_O3.png',
                'Dados/Graficos/Novo/grafico_c_subword.png',
                'Dados/Graficos/Novo/grafico_c_unroll.png',
                'Dados/Graficos/Novo/grafico_c_blocking.png',
                'Dados/Graficos/Novo/grafico_c_multithread.png']


#divide_csv_values(csv_files[0:2] + csv_files[5:10],'Dados/Graficos/Novo/resultado_tempo.csv')

def salva_graficos_individuais(csv_files,output_files,name_columns):
    for n in range(0,10):
        salva_graficos([csv_files[n]], output_files[n], [name_columns[n]])
     

salva_graficos_individuais(csv_files,output_files,name_columns)

output_files_2 = "grafico_DGEMM.png"
salva_graficos([csv_files[0:2] + csv_files[5:10]], output_files_2, name_columns[0:2] + name_columns[5:10])

'''Gráficos de speed-up'''
divide_csv_values(csv_files[0:2] + csv_files[5:10],'Dados/Resultados/resultado_tempo')
