import pandas as pd
import matplotlib.pyplot as plt

def plot_multiple_scatter_chart(csv_files, output_file, name_columns):
    # Lê os dados dos arquivos CSV e combina-os em um DataFrame único
    combined_data = pd.concat([pd.read_csv(csv_file, header=None, names=[name]) for csv_file, name in zip(csv_files, name_columns)], axis=1)

    # Configura o gráfico de dispersão
    plt.figure(figsize=(12, 8))
    for col in combined_data.columns:
        plt.scatter(combined_data.index, combined_data[col], label=col)

    plt.xlabel('Índice')
    plt.ylabel('Tempo médio (s)')
    plt.title('Tempo de Processamento - DGEMM (Scatter Plot)')
    plt.yscale('log')  # Usar escala logarítmica para melhor visualização no eixo y
    plt.legend(title='Implementações')
    plt.grid(True)

    # Salva o gráfico como um arquivo PNG
    plt.savefig(output_file, format='png')
    plt.close()  # Fecha a figura para liberar memória


# Lista de arquivos CSV
csv_files = ['Dados/tempos_medios_python_0.csv', 'Dados/tempos_medios_python_1.csv']

# Lista de nomes para as colunas
name_columns = ['Python não otimizado', 'Python com Numpy']

# Nome do arquivo de saída (gráfico PNG)
output_file = 'Dados/grafico_python_scatter.png'

# Chama a função para plotar o gráfico de dispersão comparativo e salvar como PNG
plot_multiple_scatter_chart(csv_files, output_file, name_columns)

print(f"Gráfico de dispersão salvo como {output_file}")
