import pandas as pd
import matplotlib.pyplot as plt
from PIL import Image
import os

def graficos_speed_up():
        # Carregar os dados do arquivo CSV
        data = pd.read_csv('Dados/Resultados/resultado_tempo.csv')
        data_err = pd.read_csv('Dados/Resultados/resultado_tempo_err.csv')

            # Extrair os dados de cada coluna
        ordem = data['Ordem']
        python_otimizado = data['Python otimizado']
        c_nao_otimizado = data['C não otimizado']
        c_com_subwords = data['C com subwords']
        c_com_unroll = data['C com unroll']
        c_com_multithreads = data['C com multithreads']
        
        # Extrair as incertezas de cada coluna
        python_otimizado_err = data_err['Python otimizado']
        c_nao_otimizado_err = data_err['C não otimizado']
        c_com_subwords_err = data_err['C com subwords']
        c_com_unroll_err = data_err['C com unroll']
        c_com_multithreads_err = data_err['C com multithreads']

        # Criar um gráfico separado para cada ordem de matriz
        for i in range(len(ordem)):
                plt.figure(figsize=(8, 5))
                plt.errorbar(['Python otimizado', 'C não otimizado', 'C com subwords', 'C com unroll', 'C com multithreads'],
                        [python_otimizado[i], c_nao_otimizado[i], c_com_subwords[i], c_com_unroll[i], c_com_multithreads[i]],
                        yerr=[python_otimizado_err[i], c_nao_otimizado_err[i], c_com_subwords_err[i], c_com_unroll_err[i], c_com_multithreads_err[i]],
                        marker='o', linestyle='--', capsize=5)
                plt.title(f'Ordem da Matriz: {ordem[i]}')
                plt.ylabel('Velocidade (relativa ao Python não otimizado)')
                plt.xticks(rotation=45)
                plt.tight_layout()
                plt.savefig(f'Dados/Graficos/Novo/Speed_up/ordem_linha_{ordem[i]}.png')
                plt.close()
                print(f"Gráficos salvos, ordem {i}.")
    
def combinar_imagens(diretorio, nome_saida):
    imagens = []
    for nome_arquivo in sorted(os.listdir(diretorio)):
        if nome_arquivo.endswith(".png"):
            caminho_completo = os.path.join(diretorio, nome_arquivo)
            imagens.append(Image.open(caminho_completo))
    
    largura_individual = imagens[0].width
    altura_individual = imagens[0].height
    largura_total = 2 * largura_individual
    altura_total = 3 * altura_individual
    
    imagem_combinada = Image.new("RGB", (largura_total, altura_total), color=(255, 255, 255))
    
    for i, im in enumerate(imagens):
        linha = i // 2
        coluna = i % 2
        x_offset = coluna * largura_individual
        y_offset = (2- linha) * altura_individual
        imagem_combinada.paste(im, (x_offset, y_offset))
    
    imagem_combinada.save(nome_saida)

graficos_speed_up()
combinar_imagens("Dados/Graficos/Novo/Speed_up", "Dados/Graficos/Novo/imagem_combinada.png")
print("Fim")