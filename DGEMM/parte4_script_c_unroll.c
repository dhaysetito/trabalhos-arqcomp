#include <x86intrin.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define OK 0
#define UNROLL (4)

/**/

double *matrizA, *matrizB, *matrizC;

void dgemm(int n, double* A, double* B, double* C) {
    for (int i = 0; i < n; i += UNROLL * 4) {
        for (int j = 0; j < n; ++j) {
            __m256d c[UNROLL];
            for (int r = 0; r < UNROLL; r++)
                c[r] = _mm256_load_pd(C + i + r * 4 + j * n);
            for (int k = 0; k < n; k++) {
                __m256d bb = _mm256_broadcast_sd(B + j * n + k);
                for (int r = 0; r < UNROLL; r++)
                    c[r] = _mm256_fmadd_pd(_mm256_load_pd(A + n * k + r * 4 + i), bb, c[r]);
            }
            for (int r = 0; r < UNROLL; r++)
                _mm256_store_pd(C + i + r * 4 + j * n, c[r]);
        }
    }
}

/* Cria uma matriz X de ordem n */
void criar_matriz(int n, double* A) {
    for (int i = 0; i < n * n; ++i) {
        A[i] = rand() / (double)RAND_MAX; /* Gerar valores aleatórios entre 0 e 1 */
    }
}

/* Gera e aloca 3 matrizes de ordem n*/
void criar_matrizes_input(int ordem) {
    posix_memalign((void**)&matrizA, 32, ordem * ordem * sizeof(double));
    posix_memalign((void**)&matrizB, 32, ordem * ordem * sizeof(double));
    posix_memalign((void**)&matrizC, 32, ordem * ordem * sizeof(double));

    if (matrizA == NULL || matrizB == NULL || matrizC == NULL) {
        fprintf(stderr, "Erro na alocação de memória\n");
        exit(1);
    }

    criar_matriz(ordem, matrizA);
    criar_matriz(ordem, matrizB);
}

/*Libera a memoria da matriz*/
void liberar_matrizes() {
    free(matrizA);
    free(matrizB);
    free(matrizC);
}

/* Testa se a multplicacao estah correta*/
void teste_matriz(int ordem, double* matrizA, double* matrizB, double* matrizC) {
    // Teste de matriz
    printf("Matriz A ---------- \n");
    for (int i = 0; i < ordem; i++) {
        for (int j = 0; j < ordem; j++) {
            printf("%f ", matrizA[i * ordem + j]);
        }
        printf("\n"); // Adiciona uma nova linha após cada linha da matriz 
    }
    printf("----------------------------------\n\n");
    printf("Matriz B ---------- \n");
    for (int i = 0; i < ordem; i++) {
        for (int j = 0; j < ordem; j++) {
            printf("%f ", matrizB[i * ordem + j]);
        }
        printf("\n"); // Adiciona uma nova linha após cada linha da matriz 
    }
    printf("----------------------------------");
    printf("Matriz C ---------- \n\n");

    for (int i = 0; i < ordem; i++) {
        for (int j = 0; j < ordem; j++) {
            printf("%f ", matrizC[i * ordem + j]);
        }
        printf("\n"); // Adiciona uma nova linha após cada linha da matriz 
    }
    printf("----------------------------------\n\n");
}

/* Executa a multplicacao para diferentes ordens e salvar o tempo*/
void executar_testes(int* lista, int tamanho_lista, double* tempos_medios, double* desvio_padrao) {
    for (int i = 0; i < tamanho_lista; ++i) {
        int n = lista[i];
        printf("Executando multiplicacao de matrizes de ordem %d...\n", n);

        double soma_tempos = 0.0;
        double tempos[5]; // Para armazenar os tempos de execução

        for (int j = 0; j < 5; ++j) {
            criar_matrizes_input(n);
            clock_t tempo_de_inicio = clock();
            dgemm(n, matrizA, matrizB, matrizC);
            clock_t tempo_de_execucao = clock() - tempo_de_inicio;
            soma_tempos += ((double) tempo_de_execucao) / CLOCKS_PER_SEC;
            tempos[j] = ((double) tempo_de_execucao) / CLOCKS_PER_SEC;
            liberar_matrizes();
        }

        tempos_medios[i] = soma_tempos / 5.0;

        // Calcular o desvio padrão
        double soma_quad_dif = 0.0;
        for (int k = 0; k < 5; ++k) {
            soma_quad_dif += pow(tempos[k] - tempos_medios[i], 2);
        }
        desvio_padrao[i] = sqrt(soma_quad_dif / 5.0);

        printf("Ordem %d: Tempo medio de 5 execucoes: %0.5f segundos\n", n, tempos_medios[i]);
        printf("Desvio padrao: %0.5f\n", desvio_padrao[i]);
    }
}

void salvar_tempos_em_csv(const char* nome_arquivo, int* lista_ordens, int tamanho_lista, double* tempos_medios, double* desvio_padrao) {
    FILE* arquivo = fopen(nome_arquivo, "w");
    if (arquivo == NULL) {
        fprintf(stderr, "Erro ao abrir o arquivo para escrita\n");
        exit(1);
    }

    fprintf(arquivo, "Ordem,Tempo Medio(s),Desvio padrão(s)\n");
    for (int i = 0; i < tamanho_lista; ++i) {
        fprintf(arquivo, "%d,%0.5f,%0.5f\n", lista_ordens[i], tempos_medios[i], desvio_padrao[i]);
    }

    fclose(arquivo);
    printf("Tempos medios salvos no arquivo %s\n", nome_arquivo);
}


int main() {
    int lista_ordens[] = {128, 256};
    int tamanho_lista = sizeof(lista_ordens) / sizeof(lista_ordens[0]);
    double tempos_medios[tamanho_lista];
    double desvio_padrao[tamanho_lista];

    executar_testes(lista_ordens, tamanho_lista, tempos_medios, desvio_padrao);

    salvar_tempos_em_csv("tempos_medios_c_unroll.csv", lista_ordens, tamanho_lista, tempos_medios, desvio_padrao);

    printf("Tempos medios e desvio para cada ordem de matriz: ");
    for (int i = 0; i < tamanho_lista; ++i) {
        printf("%d: %0.5f segundos (Desvio Padrao: %0.5f)", lista_ordens[i], tempos_medios[i], desvio_padrao[i]);
        if (i < tamanho_lista - 1) {
            printf(", ");
        } else {
            printf("\n");
        }
    }

    return OK;
}