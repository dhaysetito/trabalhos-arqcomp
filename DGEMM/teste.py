import math

tempos = [[0.5044288635253906, 0.5457525253295898, 0.5255649089813232, 0.534029483795166, 0.48790502548217773], [3.998969554901123, 4.064103603363037, 3.9529988765716553, 4.1500794887542725, 3.9570069313049316], [37.406723499298096, 37.870028257369995, 37.54708480834961, 37.343109369277954, 38.26733064651489], [332.5263466835022, 340.05987334251404, 330.0401840209961, 326.4165184497833, 327.52836060523987], [2794.6061737537384, 2844.909969329834, 2844.0747442245483, 2876.4824697971344, 2867.6248664855957], [33219.45278596878, 33748.997635126114, 33132.08199620247, 33043.184899806976, 33349.02785086632]]

'''Funcao que retorna tempo do DGEMM'''
def time_average(time_list):
    time_average = []
    time_std = []
    time_total_list = []
    time_total = 0
    time_new_list = [[],[],[],[],[],[]]
    
    for index, sublist in enumerate(time_list):
        for item in sublist:
            if item > 0:
                time_new_list[index].append(item)

    for i in time_new_list:
        time_aux_average = 0
        for j in i:
            time_total+=j
            time_aux_average+=j

        time_total_list.append("{:.5f}".format(time_aux_average))   #Adiciona a soma dos tempos numa lista
        time_aux_average /= len(i)  #Faz a media
        
        #Calculo do desvio padrao
        diff_2 = [(x - time_aux_average) ** 2 for x in i]
        variance = sum(diff_2) / (len(i) - 1) 
        std_average = math.sqrt(variance)

        time_average.append("{:.5f}".format(time_aux_average))
        time_std.append("{:.5f}".format(std_average))

    return time_average, time_std, time_total,time_total_list, time_new_list

time_average_list, time_std_list, time_total,time_total_list, time_new_list = time_average(tempos)

print(f"\nLista de tempos: {time_new_list} \nLista tempo médio: {time_average_list} \n" \
      f"Lista do desvio padrão: {time_std_list} \nTempo total: {time_total:.5f}\n")